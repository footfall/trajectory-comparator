#include <gtest/gtest.h>
#include "comparator.h"
#include "trajectory.h"
#include "reader.h"
#include <vector>

class TestComparator : public ::testing::Test {
protected:

    std::vector<Trajectory> readTrajectoriesFromFile(const std::string& file_name) {
        Reader reader(file_name);
        return reader.readTrajectories();
    }
};


bool containsSameTrajectories(const std::vector<Trajectory>& trajectories1, const std::vector<Trajectory>& trajectories2) {
    if (trajectories1.size() != trajectories2.size()) {
        return false;
    }

    for (const auto& trajectory1 : trajectories1) {
        bool found = false;
        for (const auto& trajectory2 : trajectories2) {
            if (trajectory1 == trajectory2) {
                found = true;
                break;
            }
        }
        if (!found) {
            return false;
        }
    }
    return true;
}



TEST_F(TestComparator, TestLengthDifferenceMetric) {
    auto trajectories = readTrajectoriesFromFile("test_trajectory.dat");
    int num_threads = std::thread::hardware_concurrency();
    Comparator comparator(trajectories, num_threads);

    Trajectory ref_trajectory;
    ref_trajectory.addPoint({0, 0, 0});
    ref_trajectory.addPoint({2, 2, 2});

    auto closest_trajectories = comparator.findClosestTrajectories(ref_trajectory, Comparator::Metric::LENGTH_DIFFERENCE, 3);

    ASSERT_EQ(closest_trajectories.size(), 2);
}

TEST_F(TestComparator, TestSpeedDifferenceMetric) {
    auto trajectories = readTrajectoriesFromFile("test_trajectory.dat");
    int num_threads = std::thread::hardware_concurrency();
    Comparator comparator(trajectories, num_threads);

    Trajectory ref_trajectory;
    ref_trajectory.addPoint({0, 0, 0});
    ref_trajectory.addPoint({2, 2, 2});

    auto closest_trajectories = comparator.findClosestTrajectories(ref_trajectory, Comparator::Metric::SPEED_DIFFERENCE, 3);

    ASSERT_EQ(closest_trajectories.size(), 2);
}

TEST_F(TestComparator, TestSmallInputFile) {
    auto trajectories = readTrajectoriesFromFile("small_trajectory.dat");
    int num_threads = std::thread::hardware_concurrency();
    Comparator comparator(trajectories, num_threads);

    Trajectory ref_trajectory;
    ref_trajectory.addPoint({0, 0, 0});
    ref_trajectory.addPoint({2, 2, 2});

    auto closest_trajectories = comparator.findClosestTrajectories(ref_trajectory, Comparator::Metric::LENGTH_DIFFERENCE, 3);

    ASSERT_EQ(closest_trajectories.size(), 3);
}

TEST_F(TestComparator, TestLargeInputFile) {
    auto trajectories = readTrajectoriesFromFile("large_trajectory.dat");
    int num_threads = std::thread::hardware_concurrency();
    Comparator comparator(trajectories, num_threads);

    Trajectory ref_trajectory;
    ref_trajectory.addPoint({0, 0, 0});
    ref_trajectory.addPoint({2, 2, 2});

    auto closest_trajectories = comparator.findClosestTrajectories(ref_trajectory, Comparator::Metric::LENGTH_DIFFERENCE, 3);

    ASSERT_EQ(closest_trajectories.size(), 3);
}


TEST_F(TestComparator, TestLengthDifferenceMetricExpectedTrajectories) {
    auto trajectories = readTrajectoriesFromFile("test_trajectory.dat");
    int num_threads = std::thread::hardware_concurrency();
    Comparator comparator(trajectories, num_threads);

    Trajectory ref_trajectory;
    ref_trajectory.addPoint({0, 0, 0});
    ref_trajectory.addPoint({2, 2, 2});

    auto closest_trajectories = comparator.findClosestTrajectories(ref_trajectory, Comparator::Metric::LENGTH_DIFFERENCE, 3);

    ASSERT_EQ(closest_trajectories.size(), 2);

    Trajectory expected_trajectory1;
    expected_trajectory1.addPoint({1, 1, 1});
    expected_trajectory1.addPoint({0, 1, 3});
    expected_trajectory1.addPoint({9, 0, 2});

    Trajectory expected_trajectory2;
    expected_trajectory2.addPoint({0, 0, 0});
    expected_trajectory2.addPoint({0, 1, 1});
    expected_trajectory2.addPoint({0, 10, 2});

    std::vector<Trajectory> expected_trajectories{expected_trajectory1, expected_trajectory2};
    EXPECT_TRUE(containsSameTrajectories(closest_trajectories, expected_trajectories));    
}

TEST_F(TestComparator, TestSpeedDifferenceMetricExpectedTrajectories) {
    auto trajectories = readTrajectoriesFromFile("test_trajectory.dat");
    int num_threads = std::thread::hardware_concurrency();
    Comparator comparator(trajectories, num_threads);

    Trajectory ref_trajectory;
    ref_trajectory.addPoint({0, 0, 0});
    ref_trajectory.addPoint({2, 2, 2});

    auto closest_trajectories = comparator.findClosestTrajectories(ref_trajectory, Comparator::Metric::SPEED_DIFFERENCE, 3);

    ASSERT_EQ(closest_trajectories.size(), 2);
    Trajectory expected_trajectory1;
    expected_trajectory1.addPoint({1, 1, 1});
    expected_trajectory1.addPoint({0, 1, 3});
    expected_trajectory1.addPoint({9, 0, 2});

    Trajectory expected_trajectory2;
    expected_trajectory2.addPoint({0, 0, 0});
    expected_trajectory2.addPoint({0, 1, 1});
    expected_trajectory2.addPoint({0, 10, 2});

    std::vector<Trajectory> expected_trajectories{expected_trajectory1, expected_trajectory2};
    EXPECT_TRUE(containsSameTrajectories(closest_trajectories, expected_trajectories));    
}
