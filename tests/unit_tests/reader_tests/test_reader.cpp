#include <gtest/gtest.h>
#include "trajectory.h"
#include "reader.h"



TEST(ReaderTest, ReadTrajectories) {
  Reader reader("test_trajectory.dat");
  std::vector<Trajectory> trajectories = reader.readTrajectories();
  ASSERT_EQ(trajectories.size(), 2);

  Trajectory& trajectory1 = trajectories[0];
  ASSERT_EQ(trajectory1.getSize(), 3);
  ASSERT_EQ(trajectory1[0].x, 1);
  ASSERT_EQ(trajectory1[0].y, 1);
  ASSERT_EQ(trajectory1[0].t, 1);
  ASSERT_EQ(trajectory1[1].x, 0);
  ASSERT_EQ(trajectory1[1].y, 1);
  ASSERT_EQ(trajectory1[1].t, 3);
  ASSERT_EQ(trajectory1[2].x, 9);
  ASSERT_EQ(trajectory1[2].y, 0);
  ASSERT_EQ(trajectory1[2].t, 2);


  Trajectory& trajectory2 = trajectories[1];
  ASSERT_EQ(trajectory2.getSize(), 3);
  ASSERT_EQ(trajectory2[0].x, 0);
  ASSERT_EQ(trajectory2[0].y, 0);
  ASSERT_EQ(trajectory2[0].t, 0);
  ASSERT_EQ(trajectory2[1].x, 0);
  ASSERT_EQ(trajectory2[1].y, 1);
  ASSERT_EQ(trajectory2[1].t, 1);
  ASSERT_EQ(trajectory2[2].x, 0);
  ASSERT_EQ(trajectory2[2].y, 10);
  ASSERT_EQ(trajectory2[2].t, 2);



}
