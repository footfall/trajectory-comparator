#include <gtest/gtest.h>
#include "trajectory.h"
#include <vector>
#include <memory>
#include <string>


TEST(TrajectoryTest, AddPoint) {
  Trajectory trajectory;
  Point point = {1, 2, 3};
  trajectory.addPoint(point);
  ASSERT_EQ(trajectory.getPoints().size(), 1);
  ASSERT_EQ(trajectory.getPoints()[0].x, 1);
  ASSERT_EQ(trajectory.getPoints()[0].y, 2);
  ASSERT_EQ(trajectory.getPoints()[0].t, 3);
}

TEST(TrajectoryTest, Distance) {
  Trajectory trajectory;
  Point p1 = {0, 0, 0};
  Point p2 = {3, 4, 5};
  double distance = trajectory.distance(p1, p2);
  ASSERT_DOUBLE_EQ(distance, 5.0);
}

TEST(TrajectoryTest, LengthDifference) {
  Trajectory trajectory1;
  Trajectory trajectory2;

  trajectory1.addPoint({0, 0, 0});
  trajectory1.addPoint({3, 4, 1});

  trajectory2.addPoint({0, 0, 0});
  trajectory2.addPoint({6, 8, 1});

  double length_difference = trajectory1.lengthDifference(trajectory2);
  ASSERT_DOUBLE_EQ(length_difference, 5.0);
}

TEST(TrajectoryTest, SpeedDifference) {
  Trajectory trajectory1;
  Trajectory trajectory2;

  trajectory1.addPoint({0, 0, 0});
  trajectory1.addPoint({3, 4, 1});

  trajectory2.addPoint({0, 0, 0});
  trajectory2.addPoint({6, 8, 2});

  double speed_difference = trajectory1.speedDifference(trajectory2);
  ASSERT_DOUBLE_EQ(speed_difference, 0.0);
}
