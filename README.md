# Footfall - Trajectory Comparison Tool

Footfall is a C++ tool for comparing trajectories in 2D space. Given a reference trajectory and a dataset of trajectories, it can find the closest trajectories to the reference based on two metrics: length difference and speed difference.

## Features

- Fast trajectory comparison using multi-threading
- Supports two metrics for comparing trajectories:
  - Length difference
  - Speed difference
- Easy-to-use command-line interface
- Input data files in a simple, human-readable format

## Requirements

- C++17 compatible compiler (e.g., GCC, Clang)
- CMake (version 3.10 or higher)

## Building the Project

1. Clone the repository:
```
git clone https://gitlab.com/footfall/trajectory-comparator.git

```

2. Enter the project directory:

```
cd trajectory-comparator
```

3. Create a build directory and enter it:

```
mkdir build && cd build

```

4. Run CMake to configure the build:

```
cmake ..

```

5. Build the project:

```
make
```

The `footfall` executable will be generated in the `build` directory.


## Usage

./footfall 

- `input_file`: Path to the input file containing the trajectory dataset. You can use files under tests/files/ directory for reference.
- `reference_trajectory_index`: Index of the reference trajectory in the input file (0-based).
- `comparison_metric`: Comparison metric to use: "length" for length difference or "speed" for speed difference.
The program will output the 3 closest trajectories.

## Input File Format

The trajectory data file should have the following format:
```
num_trajectories
num_points_1 x1 y1 t1 x2 y2 t2 ... xn yn tn
num_points_2 x1 y1 t1 x2 y2 t2 ... xn yn tn
...
num_points_N x1 y1 t1 x2 y2 t2 ... xn yn tn
```
- `num_trajectories`: The total number of trajectories in the file.
- `num_points_i`: The number of points in the ith trajectory.
- `xj yj tj`: The x, y coordinates and time for the jth point in a trajectory.



