#include "trajectory.h"
#include <iostream>

void Trajectory::addPoint(const Point& point) {
  points_.push_back(point);
}

double Trajectory::distance(const Point& p1, const Point& p2) const {
  return std::sqrt(std::pow(p2.x - p1.x, 2) + std::pow(p2.y - p1.y, 2));
}

double Trajectory::lengthDifference(const Trajectory &trajectory) const {
  double length1 = 0.0;
  double length2 = 0.0;
  for (size_t i = 1; i < points_.size(); ++i) {
    length1 += distance(points_[i - 1], points_[i]);
  }

  for (size_t i = 1; i < trajectory.points_.size(); ++i) {
    length2 += distance(trajectory.points_[i - 1], trajectory.points_[i]);
  }

  return std::abs(length1 - length2);
}

double Trajectory::speedDifference(const Trajectory& trajectory) const {
  double speed1 = 0.0;
  double speed2 = 0.0;

  for (size_t i = 1; i < points_.size(); ++i) {
    double dist = distance(points_[i - 1], points_[i]);
    double time = static_cast<double>(points_[i].t - points_[i - 1].t);
    speed1 += dist / time;
  }

  for (size_t i = 1; i < trajectory.points_.size(); ++i) {
    double dist = distance(trajectory.points_[i - 1], trajectory.points_[i]);
    double time = static_cast<double>(trajectory.points_[i].t - trajectory.points_[i - 1].t);
    speed2 += dist / time;
  }

  speed1 /= (points_.size() - 1);
  speed2 /= (trajectory.points_.size() - 1);

  return std::abs(speed1 - speed2);
}

size_t Trajectory::getSize() const {
  return points_.size();
}


const std::vector<Point>& Trajectory::getPoints() const {
  return points_;
}

const Point &Trajectory::operator[](size_t index) const {
  return points_[index];
}


bool Trajectory::operator==(const Trajectory& trajectory) const {
  const auto& trajectory_points = trajectory.getPoints();
  if (points_.size() != trajectory_points.size()) {
    return false;
  }

  for (size_t i = 0; i < points_.size(); ++i) {
    if (points_[i].x != trajectory_points[i].x || points_[i].y != trajectory_points[i].y || points_[i].t != trajectory_points[i].t) {
      return false;
    }
  }
  return true;
}


std::ostream &operator<<(std::ostream &os, const Trajectory &trajectory) {
  const auto &points = trajectory.getPoints();
  os << "Trajectory(";
  for (size_t i = 0; i < points.size(); ++i) {
    os << "Point(" << points[i].x << ", " << points[i].y << ", " << points[i].t << ")";
    if (i < points.size() - 1) {
      os << ", ";
    }
  }
  os << ")";
  return os;
}

std::istream &operator>>(std::istream& is, Trajectory& trajectory) {
  int num_points;
  is >> num_points;

  for (int i = 0; i < num_points; ++i) {
    Point point;
    is >> point.x >> point.y >> point.t;
    trajectory.addPoint(point);
  }
  return is;
}

void Trajectory::print() const {
  for (const Point& point : points_) {
    std::cout << "(" << point.x << ", " << point.y << ", " << point.t << ")\n";
  }	
}
