#include "comparator.h"
#include "trajectory.h"
#include <algorithm>
#include <functional>
#include <numeric>
#include <future>

Comparator::Comparator(const std::vector<Trajectory>& trajectories, int num_threads):
	trajectories_(trajectories), thread_pool_(num_threads) {}


std::vector<Trajectory> Comparator::findClosestTrajectories(const Trajectory& ref_trajectory, Metric metric, int num_results) {
	std::vector<double> differences(trajectories_.size());
	std::vector<int> indices(trajectories_.size());
	std::iota(indices.begin(), indices.end(), 0);

	int chunk_size = static_cast<int>(std::ceil(static_cast<double>(trajectories_.size()) / thread_pool_.getNumThreads()));

	std::vector<std::future<void>> futures;
	for (int i = 0; i < thread_pool_.getNumThreads(); ++i) {
		int start = i * chunk_size;
		int end = std::min(start + chunk_size, static_cast<int>(trajectories_.size()));
		futures.push_back(thread_pool_.enqueue([&, start, end] {
			for (int j = start; j < end; ++j) {
				if (metric == Metric::LENGTH_DIFFERENCE) {
					differences[j] = trajectories_[j].lengthDifference(ref_trajectory);
				} else {
					differences[j] = trajectories_[j].speedDifference(ref_trajectory);
				}
			}
		}));
	}

	for (auto& future : futures) {
		future.wait();
	}
	
  num_results = std::min(num_results, static_cast<int>(trajectories_.size()));
  std::partial_sort(indices.begin(), indices.begin() + num_results, indices.end(),
			[&](int a, int b) { return differences[a] < differences[b]; });
  std::vector<Trajectory> closest_trajectories;
  for (int i = 0; i < num_results; ++i) {
    closest_trajectories.push_back(trajectories_[indices[i]]);
  }
  
  return closest_trajectories;
}

