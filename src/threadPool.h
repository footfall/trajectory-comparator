#ifndef DIGEIZ_THREADPOOL_H_
#define DIGEIZ_THREADPOOL_H_

#include <mutex>
#include <condition_variable>
#include <thread>
#include <functional>
#include <future>
#include <vector>
#include <queue>


/**
 * @brief A thread pool class for parallelizing computations across multiple threads
 */
class ThreadPool {
	public:

    /**
     * @brief Constructs a thread pool with the given number of threads 
     *
     * @param num_threads The number of threads in the thread pool
     */
		ThreadPool(int num_threads) : num_threads_(num_threads), stop_(false) {
			comparators_.reserve(num_threads);
			for (int i = 0; i < num_threads; ++i) {
				comparators_.emplace_back([this]{
					while (true) {
						std::unique_lock<std::mutex> lock(task_mutex_);
						task_cv.wait(lock, [this](){return (stop_ || !tasks_.empty());});
						if (stop_ && tasks_.empty()) {
							return;
						}
						auto task = std::move(tasks_.front());
						tasks_.pop();
						lock.unlock();
						task();
					}
				});
			}
		}
		
    /**
     * @brief Destructs the thread pool
     */
		~ThreadPool() {
			shutdown();
		}
		
    /**
     * @brief Shuts down the thread pool
     */
		void shutdown() {
			{
				std::unique_lock<std::mutex> lock(task_mutex_);
				stop_ = true;
			}
			task_cv.notify_all();

			for (auto& comparator : comparators_) {
				if (comparator.joinable()) {
					comparator.join();
				}
			}
		}

    /**
     * @brief Gets the number of threads in the thread pool
     *
     * @return The number of threads in the thread pool
     */
		int getNumThreads() const {
			return num_threads_;
		}	
		
    /**
     * @brief Enqueues a function to be executed by the thread pool
     *
     * @tparam F The type of the function to be executed
     * @param f The function to be executed
     *
     * @return A future that can be used to check the status of the enqueued function
     */
		template<class F>
		auto enqueue(F &&f) -> std::future<void> {
			auto task = std::make_shared<std::packaged_task<void()>>(std::forward<F>(f));
			std::future<void> res = task->get_future();
			{
				std::unique_lock<std::mutex> lock(task_mutex_);
				if (stop_) {
					throw std::runtime_error("Enqueue while stopping threadpool");
				}
				tasks_.emplace([task]() {(*task)();});
			}
			task_cv.notify_one();
			return res;
		}

	private:

    /**
     * @brief The number of threads in the thread pool
     */
		int num_threads_;
		
    /**
     * @brief The worker threads in the thread pool
     */
    std::vector<std::thread> comparators_;
		
    /**
     * @brief The tasks to be executed by the thread pool
     */
    std::queue<std::function<void()>> tasks_;
		
    /**
     * @brief The mutex used to protect the tasks queue
     */
    std::mutex task_mutex_;
		
    /**
     * @brief The condition variable used to signal the worker threads
     */
    std::condition_variable task_cv;
		
    /**
     * @brief A flag indicating whether the thread pool should stop
     */
    bool stop_;
};


#endif
