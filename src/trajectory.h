#ifndef DIGEIZ_TRAJECTORY_H_
#define DIGEIZ_TRAJECTORY_H_

#include <vector>
#include <cmath>
#include <istream>


/**
 * @brief A point in 2D space and time. 
 */
struct Point {
	int x;
	int y;
	int t;
};

/**
 * @brief A trajectory is a list of points in time order
 */
class Trajectory {
	public:
		
    /**
     * @brief Add a point to the trajectory.
     *
     * @param point The point to add
     */
    void addPoint(const Point& point);
		
    /**
     * @brief Calculate the Euclidean distance between two points
     *
     * @param p1 The first point
     * @param p2 The second point
     *
     * @return The Euclidean distance between the two points
     */
    double distance(const Point& p1, const Point& p2) const;
		
    /**
     * @brief Calculate the difference in length between two trajectories
     *
     * @param trajectory trajectory The other trajectory to compare to
     *
     * @return The absolute difference in length
     */
    double lengthDifference(const Trajectory& trajectory) const;
		
    /**
     * @brief Calculate the difference in average speed between two trajectories
     *
     * @param trajectory The other trajectory to compare to
     *
     * @return The absolute difference in average speed
     */
    double speedDifference(const Trajectory& trajectory) const;
		
    /**
     * @brief Get the number of points in the trajectory
     *
     * @return The number of points
     */
    size_t getSize() const;
		
    /**
     * @brief Get the list of points in the trajectory
     *
     * @return The list of points
     */
    const std::vector<Point>& getPoints() const;
		
    /**
     * @brief Get a const reference to the point at the given index
     *
     * @param index The index of the point
     *
     * @return A const reference to the point at the given index
     */
    const Point& operator[](size_t index) const;
		
    
		/**
		* @brief Return if the result of comparison with another trajectory 
		*
		* @param trajectory The other trajectory to compare to 
		*
		* @return true if the two trajectories are equal else, false.
		*/
		bool operator==(const Trajectory& trajectory) const;
    
		/**
		* @brief Output a human-readable representation of a Trajectory object.
		*
		* This function is a friend of the Trajectory class and allows the output of
		* a human-readable representation of a Trajectory object to a given output
		* stream, using the '<<' operator.
		*
		* @param os The output stream to write the Trajectory object to.
		* @param trajectory The Trajectory object to output.
		* @return The modified output stream with the human-readable representation of the Trajectory object.
		*/	
		friend std::ostream &operator<<(std::ostream &os, const Trajectory &trajectory);

    /**
    * @brief Print the trajectory to standard output.
    */
    void print() const;
	
	private:

    /**
     * @brief The list of points in the trajectory
     */
		std::vector<Point> points_;
};


/**
 * @brief Read a trajectory from an input stream
 *
 * @param is The input stream to read from.
 * @param trajectory The trajectory to populate with data
 *
 * @return The input stream
 */
std::istream &operator>>(std::istream& is, Trajectory &trajectory);

#endif
