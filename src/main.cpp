/**
 * @mainpage Footfall digeiz test project
 *
 * @section overview Overview
 *
 * This program reads a set of trajectories from a file and compares them to a user-defined reference trajectory.
 * The comparison is based on the selected metric: length difference or speed difference.
 *
 * @section usage Usage
 *
 * To use the program, follow these steps:
 * 1. Compile and run the program.
 * 2. Enter the file path to the trajectory data file when prompted.
 * 3. Enter the reference trajectory by providing the number of points and the (x, y, t) coordinates for each point.
 * 4. Select the comparison metric: 0 for length difference or 1 for speed difference.
 *
 * The program will then output the N (default N=3) closest trajectories to the reference trajectory based on the selected metric.
 *
 * @section file_format File Format
 *
 * The trajectory data file should have the following format:
 * ```
 * num_trajectories
 * num_points_1 x1 y1 t1 x2 y2 t2 ... xn yn tn
 * num_points_2 x1 y1 t1 x2 y2 t2 ... xn yn tn
 * ...
 * num_points_N x1 y1 t1 x2 y2 t2 ... xn yn tn
 * ```
 * Where:
 * - num_trajectories: total number of trajectories in the file
 * - num_points_i: number of points in trajectory i
 * - xj, yj, tj: x, y, and t coordinates of point j in the trajectory
 */

#include "trajectory.h"
#include "reader.h"
#include "comparator.h"
#include <iostream>
#include <string>
#include <sstream>
#include <fstream>


std::string getFilePath() {
  std::string file_path;
  bool valid_input = false;

  while (!valid_input) {
    std::cout << "Enter the trajectory file path: ";
    std::cin >> file_path;

    std::ifstream file(file_path);
    if (file.good()) {
      valid_input = true;
    } else {
      std::cout << "Invalid file path, please try again.\n";
      std::cin.clear();
      std::cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
    }
  }

  return file_path;
}

Trajectory getReferenceTrajectory() {
  Trajectory reference_trajectory;

  std::cout << "Enter the number of points in the reference trajectory: ";
  int num_points;
  std::cin >> num_points;
  std::cin.ignore(); 
  int points_entered = 0;
  for (int i = 0; i < num_points; i++) {
    std::cout << "Enter x, y, and t for point " << i + 1 << " (separated by spaces or commas): ";
    std::string line;
    std::getline(std::cin, line);
    std::replace(line.begin(), line.end(), ',', ' '); 
    std::istringstream iss(line);
    Point point;
    if (iss >> point.x >> point.y >> point.t) {
      reference_trajectory.addPoint(point);
      points_entered++;
    } else {
      std::cerr << "Invalid input format. Please enter x, y, and t separated by spaces or commas." << std::endl;
    }
  }
  if (reference_trajectory.getPoints().empty()) {
    std::cerr << "Error: The reference trajectory must have at least one point." << std::endl;
    return getReferenceTrajectory();	
  }
  return reference_trajectory;
}


Comparator::Metric getComparisonMetric() {
  int metric;
  bool valid_input = false;

  while (!valid_input) {
    std::cout << "Select the comparison metric:\n";
    std::cout << "0 - Length difference metric\n";
    std::cout << "1 - Speed difference metric\n";

    std::cin >> metric;

    if (metric == 0 || metric == 1) {
      valid_input = true;
    } else {
      std::cout << "Invalid input, please enter 0 or 1.\n";
      std::cin.clear();
      std::cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
    }
  }

  return static_cast<Comparator::Metric>(metric);
}

int main() {
  std::string file_path = getFilePath();

  Reader reader(file_path);
  std::vector<Trajectory> trajectories;
  try {
    trajectories = reader.readTrajectories();
  } catch (const std::runtime_error& e) {
    std::cerr << "Error reading trajectories: " << e.what() << std::endl;
    return 1;
  }

  int num_thread = std::thread::hardware_concurrency();
  Comparator comparator(trajectories, num_thread);
  Trajectory reference_trajectory = getReferenceTrajectory();	
  Comparator::Metric metric = getComparisonMetric();
  std::cin.ignore();

  int num_results  = 3;
  std::vector<Trajectory> closest_trajectories;
  closest_trajectories = comparator.findClosestTrajectories(reference_trajectory, static_cast<Comparator::Metric>(metric), num_results);
  std::cout << "The " << num_results << " closest trajectories to the reference trajectory are:\n";
  for (Trajectory& trajectory : closest_trajectories) {
    trajectory.print();
    std::cout << std::endl;
  }
  std::cout << std::endl;

  return 0; 
}
