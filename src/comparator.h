#ifndef DIGEIZ_COMPARATOR_H_
#define DIGEIZ_COMPARATOR_H_

#include <vector>
#include "trajectory.h"
#include "threadPool.h"



/**
 * @brief A comparator for finding the closest trajectories to a reference trajectory. 
 */
class Comparator {
	public:

    /**
     * @brief Create a new comparator with a list of trajectories and a number of threads.
     *
     * @param trajectories trajectories The list of trajectories to compare.
     * @param num_threads The number of threads to use for comparisons.
     */
		Comparator(const std::vector<Trajectory>& trajectories, int num_threads);
		
    /**
     * @brief The metric used for comparison.
     */
		enum Metric {
			LENGTH_DIFFERENCE = 0, 
			SPEED_DIFFERENCE
		};


    /**
     * @brief Find the closest trajectories to a reference trajectory.
     *
     * @param ref_trajectory ref_trajectory The reference trajectory to compare to.
     * @param metric The metric to use for comparison.
     * @param num_results The number of closest trajectories to return
     *
     * @return A vector of the closest trajectories
     */
		std::vector<Trajectory> findClosestTrajectories(const Trajectory& ref_trajectory, Metric metric, int num_results);

	private:

    /**
     * @brief The vector of trajectories to compare against
     */
		std::vector<Trajectory> trajectories_;
		
    /**
     * @brief The thread pool used for the computation
     */
    ThreadPool thread_pool_;
};


#endif
