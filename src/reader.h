#ifndef DIGEIZ_READER_H_
#define DIGEIZ_READER_H_

#include "trajectory.h"
#include <string>
#include <vector>

/**
 * @brief A class for reading trajectories from a file.
 */
class Reader {
	public:

    /**
     * @brief Constructs a reader object to read trajectories from the specified file.
     *
     * @param file_path The path to the file containing the trajectories
     */
		Reader(const std::string& file_path) : file_path_(file_path) {}
		
    /**
     * @brief Reads all trajectories from the file
     *
     * @return A vector containing all the trajectories read from the file.
		 * @throw std::runtime_error If the file cannot be opened or has invalid format
     */
    std::vector<Trajectory> readTrajectories();
	private:

    /**
     * @brief The path to the file containing the trajectories.
     */
		std::string file_path_;
};

#endif
