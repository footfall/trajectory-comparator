#include "reader.h"
#include "trajectory.h"
#include <iostream>
#include <fstream>

std::vector<Trajectory> Reader::readTrajectories() {
  std::vector<Trajectory> trajectories;
  std::ifstream input_file(file_path_);
  
  std::cout << "trying to read file " << file_path_ << std::endl;
  if (!input_file.is_open()) {
    throw std::runtime_error("Could not open the file.");
  }

  int num_trajectories;
  input_file >> num_trajectories;

  for (int i = 0; i < num_trajectories; i++) {
    int num_points;
    input_file >> num_points;
    
    if (input_file.fail()) {
      throw std::runtime_error("Invalid number of points format.");
    }

    Trajectory trajectory;
    for (int j = 0; j < num_points; j++) {
      Point point;
      input_file >> point.x >> point.y >> point.t;

      if (input_file.fail()) {
        throw std::runtime_error("Invalid point format.");
      }

      trajectory.addPoint(point);
    }

    trajectories.push_back(trajectory);
  }

  return trajectories;
}

